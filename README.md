# Welcome to the Open Science Freelancers website project!
:tada: :wave: :wink:

## Making edits to the website

In the following I will explain how to make edits to the website using the
terminal.

1. Clone the project :arrow_down:
```
git clone git@gitlab.com:open-science-freelancers/open-science-freelancers.gitlab.io.git
```

2. Enter the project directory
```
cd open-science-freelancers.gitlab.io
```

3. Create a branch for your change (replace `my-new-branch` with a useful branch
name) :tanabata_tree:
```
git checkout -b my-new-brach
```

4. Now make the changes in the files you want to change.

5. Optional: install [jekyll](https://jekyllrb.com/docs/) and all dependencies for the website (`bundle install`) and check what the website looks like 
```
bundle exec jekyll serve
```
The output tells you (a) if everything went ok, and (b) how to look at the
website (for me it is `http://127.0.0.1:4000/`, which I can open in any
browser). Go back to step 4 until everything is to your liking. 

6. Commit and push your changes :white_check_mark:. You might need
to stop (`control + c`) jekyll before being able to do this.
In this example, I am assuming you changed the file `_pages/home.md` 
```
git add _pages/home.md
git commit -m "Update home"
git push --set-upstream origin my-new-branch
```  

7. Now go to https://gitlab.com/open-science-freelancers/open-science-freelancers.gitlab.io/-/merge_requests and create a merge request. Once the merge request is accepted, your changes will go live under https://open-science-freelancers.gitlab.io/. :tada:


## Adding your profile to the website

You are also an Open Science Freelancer and want to join the website? 
Welcome :wave: :smiley:

There are two ways to be added to the website:

### Prefered way: add yourself 

Go through the steps of "Making edits to the website" described above.
In step 4 do the following:

- Add a picture of yourself under `assets/images/`and name it `Firstname-Lastname.png`/`Firstname-Lastname.jpeg` (example: `Heidi-Seibold.png`)
- Change the file `_pages/home.md`: under `feature_row` add a bullet point with your info. Use the other bullet points as examples. The general structure is as follows:
```
  - image_path: /assets/images/Firstname-Lastname.png
    alt: "Firstname"
    title: "Firstname Lastname"
    excerpt: "
    **Open Science Expertise**: Your expertise <br/><br/>
    **Services**: Your services
    "
    url: "/experts/FirstnameLastname"
    btn_class: "btn--primary"
    btn_label: "More"
```
- Add a file under `_experts/`. Follow the [template](_experts/Profile_template.md) (see `_experts/Profile_template.md`) and add your information.

### Alternative way: ask Heidi to add you
Send an E-Mail to [Heidi](https://heidiseibold.gitlab.io/impressum/) and give her your Name, picture, and a short info about you (use the info we have for other trainers as examples). 




