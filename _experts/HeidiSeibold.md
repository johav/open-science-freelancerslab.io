---
title: "Heidi Seibold"
excerpt: "Open and Reproducible Data Science"
header:
  teaser: assets/images/Heidi-Seibold.png
sidebar:
  - title: "Homepage"
    image: assets/images/Heidi-Seibold.png
    image_alt: "Heidi"
    text: "[heidiseibold.gitlab.io](https://heidiseibold.gitlab.io/)"
  - title: "Contact"
    text: "[heidi@seibold.co](mailto:heidi@seibold.co)"
  - title: "Languages"                       
    text: "English, German"
---
### Open Science expertise:
Reproducibility, Open Data, Open Source, Research Software Engineering 

### Services: 
Training, Consultancy, Speaking, Event Moderation

### Research background: 
Machine Learning, Statistics, Health Research

### Training expertise: 
I offer workshops and trainings on Reproducible Data Science and on Open Science.

All trainings are interactive and no workshop is exactly like the other,
because I adapt it to you and your needs. 

### Consultancy expertise: 
Let me help you get the best out of your projects. May it be about data science, open science or reproducibility.

### Additional info: 
I am also a speaker, conference moderator and podcast host. Check out my [website](https://heidiseibold.gitlab.io/) for more details.

City: Munich <br/> 
Country: Germany <br/>
&#x2611; Happy to offer services virtually <br/>
&#x2611; Happy to travel to places I can reach by train <br/>
{: .notice}
