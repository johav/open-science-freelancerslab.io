---
title: "Jo Havemann"
excerpt: "CEO, Consultant, Entrepreneur, Mentor, Researcher, Trainer"
header:
  teaser: assets/images/Jo-Havemann.png
sidebar:
  - title: "Homepage"
    image: assets/images/Jo-Havemann.png
    image_alt: "Jo"
    text: "[https://access2perspectives.org/team/jo-havemann/](https://access2perspectives.org/team/jo-havemann/)"
  - title: "Contact"
    text: "[info@access2perspectives.org](mailto:info@access2perspectives.org)"
  - title: "Languages"                       
    text: "German, English, Swedish, French, kiSwahili"
---

Page under construction.

