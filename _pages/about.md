---
permalink: /about/
layout: splash
header:
  overlay_color: "#5e616c"
  overlay_image: /assets/images/os-freelancers-header-dots.png
excerpt: Open Science = good science in a digitized world
author_profile: false
title: "About"
---


We are freelancers working in the field of Open Science and good scientific
practice.

You can book us for trainings, consulting, projects, and more. Each of us is
an expert but our expertise varies.

## Looking for an expert?
Check out our individual profiles on this website to find out more about each
of us. You can message each person directly or [contact us](/contact) and we
will refer you to the best person for your specific question or task.

## Join our community!
Open Science Freelancers is also a community. If you are or want to become an
Open Science Freelancer, [get in touch](/contact)! We offer networking
opportunities, collaborations, visibility, and jobs.
