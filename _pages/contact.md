---
permalink: /contact/
layout: splash
header:
  overlay_color: "#5e616c"
  overlay_image: /assets/images/os-freelancers-header-dots.png
author_profile: false
title: "Contact"
---

Would you like to hire one of our freelancers? 
Would you like to get involved in our community?
Do you have questions about what we do?
{: .notice}

[Contact us!](mailto:heidi@seibold.co){: .btn .btn--primary}

---
When messaging us about required services, please provide the following information:
- What's the job (e.g. training, consultancy, research project)?
- How many people do you need?
- When and how many days do you need us for?
- Where is the job (if it's not remote/ virtual)?
- Further infos / links (to event, tender documents, ...)


