---
layout: splash
permalink: /
hidden: true
header:
  overlay_color: "#5e616c"
  overlay_image: /assets/images/os-freelancers-header-triangle.png
excerpt: Need help with Open Science? Get it here!
feature_row:
  - image_path: /assets/images/Verena-Heise.jpeg
    alt: "Verena"
    title: "Verena Heise"
    excerpt: "
	**Open Science Expertise**: Open Access Publishing, Open Data/ Methods, Reproducibility/ Replicability, Research Culture, Research(er) Assessment <br/><br/> 
	**Services**: Speaking, Training, Consultancy, Event Organisation, Event Moderation, Research 
    "
    url: "/experts/VerenaHeise"
    btn_class: "btn--primary"
    btn_label: "More"
  - image_path: /assets/images/Heidi-Seibold.png
    alt: "Heidi"
    title: "Heidi Seibold"
    excerpt: "
    **Open Science Expertise**: Reproducibility, Open Data, Open Source, Research Software Engineering  <br/><br/>
    **Services**: Training, Consultancy, Speaking, Event Moderation
    "
    url: "/experts/HeidiSeibold"
    btn_class: "btn--primary"
    btn_label: "More"
  - image_path: /assets/images/Jo-Havemann.png
    alt: "Jo"
    title: "Jo Havemann"
    excerpt: "
    **Open Science Expertise**: Open Access Publishing <br/><br/>
    **Services**: Consultancy, Mentoring, Research, Training."
    url: "/experts/JoHavemann"
    btn_class: "btn--primary"
    btn_label: "More"
---

# Our experts

{% include feature_row %}
